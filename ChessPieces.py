from enum import Enum
from abc import ABC, abstractmethod

Colour = Enum('Colour', 'white black')

import ChessImpl


class Piece(ABC):
    """Abstract base class for a chess piece.
    
    Initialize one of its subclasses instead: Queen, Knight, Bishop, or Rook.
    """

    def __init__(self, x, y, c=Colour.white):
        """Create a chess piece of colour c at the 0-based coordinates (x, y).
        
        x and y must be integers in the range [0, 7].
        c must be a `Colour`.
        """
        self.x = x
        self.y = y
        self.c = c

    def __repr__(self):
        return '%s(%d, %d, %s)' % (type(self).__name__, self.x, self.y, self.c)

    @property
    def symbol(self):
        s = self._symbol
        if self.c == Colour.white:
            return s.upper()
        else:
            return s.lower()

    @abstractmethod
    def attack(self, view):
        """Calculate the number of pieces on the ChessView that this piece can
        attack as a tuple `(white_pieces_attacked, black_pieces_attacked)`.
        """
        return None


class Queen(Piece):
    _symbol = 'Q'

    def attack(self, view):
        return ChessImpl.attack_queen(view, self.x, self.y)


class Knight(Piece):
    _symbol = 'K'

    def attack(self, view):
        return ChessImpl.attack_knight(view, self.x, self.y)


class Bishop(Piece):
    _symbol = 'B'

    def attack(self, view):
        return ChessImpl.attack_bishop(view, self.x, self.y)


class Rook(Piece):
    _symbol = 'R'

    def attack(self, view):
        return ChessImpl.attack_rook(view, self.x, self.y)
