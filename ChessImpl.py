"""Implementation of chessboard calculations. Here be dragons."""

# import intbitset
import collections
from ChessPieces import Colour


# Indices and coordinates

def board_index(x, y):
    return (y << 3) | x


def valid_point(x, y):
    return (0 <= x < 8) and (0 <= y < 8)


all_locations = set(range(64))


# Create views

ChessView = collections.namedtuple('ChessView', 'all sw sb')


def make_view(pieces):
    sw = set()
    sb = set()
    for p in pieces:
        pos = board_index(p.x, p.y)
        if p.c == Colour.white:
            sw.add(pos)
        else:
            sb.add(pos)
    return ChessView(sw | sb, sw, sb)


def print_setboard(s):
    for off in range(56, -1, -8):
        print(' '.join('X' if i in s else '.' for i in range(off, off + 8)))


def free_spaces(v):
    return list(all_locations - v.all)


# Attack masks

def rank_mask(_, y):
    y8 = y << 3
    return set(range(y8, y8 + 8))


def file_mask(x, _):
    return set(range(x, 64, 8))


def diagonal_mask(x, y):
    d = y - x
    if d < 0:
        return set(range(-d, (d + 8) << 3, 9))
    else:
        return set(range(d << 3, 64, 9))


def adiagonal_mask(x, y):
    s = x + y
    if s < 8:
        return set(range(s, (s << 3) + 1, 7))
    else:
        return set(range(((s - 6) << 3) - 1, 64, 7))


# Calculate attacks

def sum_attacks(v, x, y, mask_fns):
    """Calculate `(white_pieces_attacked, black_pieces_attacked)`"""
    occ = v.sw | v.sb
    pos = board_index(x, y)
    blk = set()
    for mask_fn in mask_fns:
        sel = occ & mask_fn(x, y)
        up, dn = 64, -1
        for i in sel:
            if i > pos and i < up: up = i
            if i < pos and i > dn: dn = i
        if up < 64: blk.add(up)
        if dn > -1: blk.add(dn)
    return (len(blk & v.sw), len(blk & v.sb))


# Piece attacks

def attack_queen(v, x, y):
    return sum_attacks(v, x, y,
                       (rank_mask, file_mask, diagonal_mask, adiagonal_mask))


def attack_bishop(v, x, y):
    return sum_attacks(v, x, y, (diagonal_mask, adiagonal_mask))


def attack_rook(v, x, y):
    return sum_attacks(v, x, y, (rank_mask, file_mask))


def attack_knight(v, x, y):
    mask = set()
    for i in (-1, 1):
        for j in (-2, 2):
            x1, y1 = x + i, y + j
            x2, y2 = x + j, y + i
            if valid_point(x1, y1):
                mask.add(board_index(x1, y1))
            if valid_point(x2, y2):
                mask.add(board_index(x2, y2))
    return (len(mask & v.sw), len(mask & v.sb))
