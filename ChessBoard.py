from ChessPieces import Piece, Queen, Knight, Bishop, Rook, Colour
import ChessImpl
import itertools
import random


class ChessBoard:
    """A representation of a chessboard."""

    _all_locations = list(itertools.product(range(8), range(8)))

    def __init__(self, pieces):
        """Create a chessboard out of a collection of Pieces.
        
        Each piece must have a unique location.
        """
        self._pieces = tuple(p for p in pieces)
        self._view = ChessImpl.make_view(self._pieces)

    def __repr__(self):
        return 'ChessBoard(%s)' % repr(self._pieces)

    @classmethod
    def create_random(cls, piece_specs):
        """Create a random chessboard from a list of piece specifications.
        
        Example:
        ```
        create_random([(Colour.white, Queen, 2), (Colour.white, Bishop, 2),
                       (Colour.white, Rook, 2), (Colour.white, Queen, 2),
                       (Colour.black, Knight, 1), (Colour.black, Queen, 2)])
        ```
        """
        total = sum(map(lambda spec: spec[2], piece_specs))
        coords = random.sample(cls._all_locations, total)
        pieces = []
        j = 0
        for (colour, ctor, n) in piece_specs:
            for i in range(n):
                x, y = coords[j]
                pieces.append(ctor(x, y, colour))
                j += 1
        return cls(pieces)

    @property
    def pieces(self):
        """Returns a tuple containing all pieces on this board.
        
        DO NOT modify the `ChessPiece`s returned.
        """
        return self._pieces

    def as_grid(self, space=''):
        grid = [['.'] * 8 for _ in range(8)]
        for piece in self._pieces:
            grid[7 - piece.y][piece.x] = piece.symbol
        return '\n'.join(space.join(row) for row in grid)

    def move_piece(self, piece, new_x, new_y):
        """Return a new ChessBoard with the specified ChessPiece moved to
        (new_x, new_y). The original is not modified.
        """
        pn = type(piece)(new_x, new_y, piece.c)
        return ChessBoard(p if p is not piece else pn for p in self._pieces)

    def free_spaces(self):
        """Return a set of free (unoccupied) squares on the board."""
        return ChessImpl.free_spaces(self._view)

    def random_loc(self):
        """Return a random unoccupied location on the board, or None."""
        spaces = self.free_spaces()
        if not spaces:
            return None
        return random.choice(spaces)

    def free_neighbours(self, x, y):
        """Return the free neighbours of the square at (x, y)."""
        spaces = self.free_spaces()
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                x1, y1 = x + dx, y + dy
                if ChessImpl.valid_point(x1, y1) and \
                   ChessImpl.board_index(x1, y1) in spaces:
                    yield x1, y1

    def evaluate(self):
        """Count the number of attacking pairs of the same side and of
        opposite sides.
        
        Returns a tuple `(same_side_attacks, opposite_side_attacks)`
        """
        own_side, opp_side = 0, 0
        for p in self._pieces:
            w, b = p.attack(self._view)
            if p.c == Colour.white:
                own_side += w
                opp_side += b
            else:
                own_side += b
                opp_side += w
        return own_side, opp_side

    def score(self):
        """Evaluate the board and return an 18-bit score. Higher scores are
        better.
        """
        own_side, opp_side = self.evaluate()
        assert 0 <= own_side <= 420 and 0 <= opp_side <= 420
        return ((420 - own_side) << 9) | opp_side
